window.addEventListener("load", (event) => {
  const time = document.getElementById("time"),
    name = document.getElementById("name"),
    greeting = document.getElementById("greeting"),
    focus = document.getElementById("focus"),
    focusHead = document.getElementById("focus-head");
  focusHead.innerHTML = "What is your focus today?";

  function showTime() {
    const today = new Date();
    hour = today.getHours();
    min = today.getMinutes();
    sec = today.getSeconds();

    //setting am/pm
    const amPm = hour >= 12 ? "PM" : "AM";

    hour %= 12 || 12;
    min = min <= 9 ? `0${min}` : min;
    sec = sec <= 9 ? `0${sec}` : sec;

    time.innerHTML = `${hour}<span>:</span>${min}<span>:</span>${sec}<span> </span>${amPm}`;

    setTimeout(showTime, 1000);
  }

  //setting backgroundImage
  function setBackround() {
    const time = new Date();
    hour = time.getHours();
    if (hour < 12) {
      greeting.innerHTML = "Good Morning";
      document.body.style.backgroundImage = "url('/Assets/morning.jpg')";
    } else if (hour < 18) {
      document.body.style.backgroundImage = "url('/Assets/afternoon.jpg')";
      greeting.innerHTML = "Good Afternoon";
    } else {
      document.body.style.backgroundImage = "url('/Assets/night.jpg')";
      greeting.innerHTML = "Good Evening";
      document.body.style.color = "white";
    }
  }

  //Getting and setting name to

  function getName() {
    if (
      localStorage.getItem("name") === "null" ||
      localStorage.getItem("name") === null
    ) {
      name.textContent = "[Enter Name]";
    } else {
      name.textContent = localStorage.getItem("name");
    }
  }

  function setName() {
    let name = prompt("What is your name?");
    if (name === "") {
      return;
    } else {
      localStorage.setItem("name", name);
      getName();
    }
  }
  //Getting and setting focus

  function getFocus() {
    if (
      localStorage.getItem("focus") === "null" ||
      localStorage.getItem("focus") === null
    ) {
      focus.textContent = "[Enter Focus]";
    } else {
      focus.textContent = localStorage.getItem("focus");
    }
  }
  function setFocus() {
    let focus = prompt("What is your focus today?");
    if (focus === "") {
      return;
    } else {
      localStorage.setItem("focus", focus);
      getFocus();
    }
  }

  name.addEventListener("click", setName);
  focus.addEventListener("click", setFocus);

  showTime();
  setBackround();
  getName();
  getFocus();
});
